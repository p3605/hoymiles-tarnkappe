# basierend auf meinen Quellen für den hoymiles-limiter und
# den Änderungen von Selbtbau-PV habe ich den limiter umgeschrieben
# für den Betrieb auf einem RP2040, Pico-W und api statt mqtt
# MIT-Lizenz, copyright Peter Fürle pf@nc-x.com
# V1.95 mit Unterscheidung 1 oder mehrere Strings
# angepasst werden müssen Credentials, IP-Adressen und True/False in den Zeilen:
# 26-27, 31-32, 36-64
# Bibliotheken laden
# MicroPython v1.19.1-896-g2e4dda3c2 on 2023-02-25; Raspberry Pi Pico W with RP2040
import machine
import network
import rp2
import utime as time
import urequests as requests
from machine import Pin
import gc
#import random # nur für Testzwecke bei Nacht

#soll debug Modus aktiviert werden?
debug = False
# ein paar Lebenszeichen ausgeben, bei kurzem Doppelblinken wurde Limit gesetzt
led_onboard = Pin("LED", Pin.OUT)
led_onboard.on()

# WLAN-Konfiguration
wlanSSID = 'ssid'
wlanPW = 'pass'
rp2.country('DE')

# Limiter-Genauigkeit beeinflussen:
hysterese = 40  # in Watt
wartezeit = 5   # in Sekunden

# Configure grid info, shelly3EM, Fronius Smartmeter oder Volkszähler vz
# aktuellen Hausverbrauch vom Shelly holen
shelly = False
if shelly: 
    shellyIP = '192.168.188.34' #IP Adresse von Shelly 3EM
    print(f"verwende Shelly an IP {shellyIP}")
# aktuellen Hausverbrauch vom Volkszähler holen
vz = True
if vz: 
    vz_URL = "http://192.168.200.207:8080"  # miniweb des Volkszähler im Chalet
    print(f"verwende Volkszähler URL {vz_URL}")
#aktuellen Hausverbrauch vom Fronius Smartmeter holen
fronius = False
if fronius: 
    fronius_url = 'http://192.168.22.188/solar_api/v1/GetPowerFlowRealtimeData.fcgi'
    print(f"verwende Fronius URL {fronius_URL}")
# Configure InfluxDB connection variables
influx = True # True oder False, gibt es überhaupt eine Influx-Anbindung?
database = 'dtu'
measurement_name = 'dtutest'
influx_url = 'http://192.168.22.210:8086/write?db='+database+'&precision=s'
if influx:
    print(f"verwende InfluxDB URL {influx_url}")
else:
    print("keine Anbindung an InfluxDB!")
# Seriennummern der Hoymiles Wechselrichter
maximum_wr = 400            # was kann das Modul / WR max abgeben
serial = "112174217xyz"     # Seriennummer des Wechselrichters
dtuIP = '192.168.20.147'    # IP Adresse von OpenDTU
dtuNutzer = 'admin'         # OpenDTU Nutzername
dtuPasswort = 'openDTU42'   # OpenDTU Passwort
# welche Art von Wechselrichtern haben wir vorliegen ?
multiMPPT = False
# in die Influx-Datenbank schreiben
def add(name,wert):
    if influx:
        #curl -i -XPOST 'http://hoy:miles@192.168.22.210:8086/write?db=dtu&precision=m' --data-binary 'dtutest limit_alt=100'
        data = measurement_name
        data += f" {name}={wert}"
        if debug: print(data)
        headers = {'Content-Type': 'application/x-www-form-urlencoded'}
        try:
            response = requests.post(influx_url, headers=headers, data=data)
        except Exception as e:
            print("Error sending data to InfluxDB: ", e)
        else:
            if response.status_code != 204:
                print("Error sending data to InfluxDB. Response code: ", response.status_code)
            else:
                if debug: print("Data sent successfully to InfluxDB.")
                if debug: print(" ")
            response.close()
    else:
       print("InfluxDB ist nicht als True definiert! Zeile 40 im Code")

# Funktion: WLAN-Verbindung, läuft am besten mit 2s Wartezeit
def wlanConnect():
  wlan = network.WLAN(network.STA_IF)
  print('WLAN-Verbindung aufbauen')
  #time.sleep(2)
  if not wlan.isconnected():
    wlan.active(True)
    wlan.connect(wlanSSID, wlanPW)
    for i in range(25):
      #if wlan.status() < 0 or wlan.status() >= 3:
      if not wlan.status() == 3:
        #break
        led_onboard.toggle()
        print('WLAN-Status ->', wlan.status())
        time.sleep(0.5)
  if wlan.isconnected():
    print('WLAN-Verbindung ok')
    led_onboard.on()
    print('WLAN-Status:', wlan.status())
    netConfig = wlan.ifconfig()
    if debug: print('IPv4-Adresse:', netConfig[0], '/', netConfig[1])
    if debug: print('Standard-Gateway:', netConfig[2])
    if debug: print('DNS-Server:', netConfig[3])
    time.sleep(2)
    led_onboard.off()
  else:
    print('Keine WLAN-Verbindung')
    led_onboard.off()
    print('WLAN-Status:', wlan.status())

# ein Doppelblinken der Onboard-LED ausgeben
def blink_double():
        led_onboard.on()
        time.sleep(0.3)
        led_onboard.off()
        time.sleep(0.1)
        led_onboard.on()
        time.sleep(0.3)
        led_onboard.off()

# WLAN-Verbindung herstellen
wlanConnect()
print("verwende DtuIP:",dtuIP)
# watchdog resettet den pico wenn was nicht stimmen sollte.
#watchdog = machine.WDT(timeout=8388)  # es sind maximal etwa 8s möglich.
while True:
     # Nimmt Daten von der openDTU Rest-API und übersetzt sie in ein json-Format
    try:
        r = requests.get("http://"+dtuIP+"/api/livedata/status/inverters")
        j_data = r.json()
    except Exception as e:
        print(f'Error connecting to openDTU: {e} at IP:{dtuIP}')
        reachable = False
    else:
        if r.status_code != 200:
            print(f'receiving unexpected data from DTU. Response code: {r.status_code} from IP:{dtuIP}')
            print('waiting 10 sec')
            time.sleep(10) # Verzögerung damit nicht unmittelbar sofort wieder abgefragt wird.
        else:
            if debug: print("got data from openDTU.")
        r.close()
        #erstmal kurz die JSON-Menge reduzieren
        out=j_data['inverters'][0]
        if '1' in out:       # gibt es mehrere MPPT ?
            multiMPPT = True

        # Selektiert spezifische Daten aus der json response
        # gilt aktuell nur für Wechselrichter mit einem MPPT HM300, HM400
        # und nur für den ersten Wechselrichter in der DTU
        # wenn der key ['inverters'][0]['1'] NICHT existiert
        if not multiMPPT:
            data_age       = j_data['inverters'][0]['data_age']                  # wie lang ist die Abfrage her ?
            benennung      = j_data['inverters'][0]['name']                      # name des Wechselrichters
            j_serial       = j_data['inverters'][0]['serial']                    # Serien# des Wechselrichters
            print("Wechselrichter mit nur 1 MPPT", benennung, j_serial)
            reachable      = j_data['inverters'][0]['reachable']                 # ist DTU erreichbar ?
            producing      = int(j_data['inverters'][0]['producing'])            # produziert der Wechselrichter etwas ?
            altes_limit    = int(j_data['inverters'][0]['limit_absolute'])       # wo war das alte Limit gesetzt
            power_dc       = j_data['inverters'][0]['AC']['0']['Power DC']['v']  # Lieferung DC vom Panel
            voltage_dc     = j_data['inverters'][0]['DC']['0']['Voltage']['v']   # Spannung DC vom Panel
            einstrahlung   = j_data['inverters'][0]['DC']['0']['Irradiation']['v']  # einstrahlung in Prozent
            power          = j_data['inverters'][0]['AC']['0']['Power']['v']     # Abgabe BKW AC in Watt
            temperature    = j_data['inverters'][0]['INV']['0']['Temperature']['v']  # Temperatur Wechselrichter
            tagesleistung  = j_data['total']['YieldDay']['v']                    # Tagesleistung aller Strings
            gesamtleistung = j_data['total']['YieldTotal']['v']                  # Gesamtleistung aller Strings
            if debug: print("altes Limit eingelesen von DTU: ",altes_limit)
        else:
            # gilt für Wechselrichter mit mehreren MPPTs ab HM600 aufwärts
            # wenn der key ['inverters'][0]['1'] existiert
            data_age       = j_data['inverters'][0]['data_age']                  # wie lang ist die Abfrage her ?
            benennung      = j_data['inverters'][0]['name']                      # name des Wechselrichters
            j_serial       = j_data['inverters'][0]['serial']                    # Serien# des Wechselrichters
            print("Wechselrichter mit mehreren MPPT", benennung, j_serial)
            reachable      = j_data['inverters'][0]['reachable']                 # ist DTU erreichbar ?
            producing      = int(j_data['inverters'][0]['producing'])            # produziert der Wechselrichter etwas ?
            altes_limit    = int(j_data['inverters'][0]['limit_absolute'])       # wo war das alte Limit gesetzt
            power_dc       = j_data['inverters'][0]['0']['Power DC']['v']        # Lieferung DC vom Panel
            voltage_dc     = j_data['inverters'][0]['0']['Voltage']['v']         # Spannung DC vom Panel
            einstrahlung   = j_data['inverters'][0]['0']['Efficiency']['v']      # einstrahlung in Prozent
            #power          = j_data['inverters'][0]['0']['Power']['v']           # Abgabe BKW AC in Watt
            temperature    = j_data['inverters'][0]['INV']['0']['Temperature']['v']  # Temperatur Wechselrichter
            power          = j_data['total']['Power']['v']                       # Abgabe BKM AC aus allen Strings
            tagesleistung  = j_data['total']['YieldDay']['v']                    # Tagesleistung aller Strings
            gesamtleistung = j_data['total']['YieldTotal']['v']                  # Gesamtleistung aller Strings
            if debug: print("altes Limit eingelesen von DTU: ",altes_limit)

    # Hausverbrauch feststellen:
    if shelly:
        # Nimmt Daten von der Shelly 3EM Rest-API und übersetzt sie in ein json-Format
        phaseA      = requests.get(f'http://{shellyIP}/emeter/0', headers={"Content-Type": "application/json"}).json()['power']
        phaseB      = requests.get(f'http://{shellyIP}/emeter/1', headers={"Content-Type": "application/json"}).json()['power']
        phaseC      = requests.get(f'http://{shellyIP}/emeter/2', headers={"Content-Type": "application/json"}).json()['power']
        grid_sum    = phaseA + phaseB + phaseC # Aktueller Bezug vom Grid - rechnet alle Phasen zusammen
    if vz:
        # Zugang über das Volkszähler MiniWeb
        try:
            data_vz = requests.get(vz_URL).json()  # Volkszähler lesen
        except Exception as e:
            print(f'Error with  {vz_URL}')
            print("Error receiving grid_sum from VZ: ", e)
            grid_sum = 0 # Notausstieg 
            wlanConnect()
        else:        
            if debug: print(data_vz)
            grid_sum = int(data_vz['data'][1]['tuples'][0][1])
    if fronius:
        # Fronius PowerMeter-Info mit aktuellem Verbrauch als Summe 
        try:
            data_fr = requests.get(fronius_url).json()
        except Exception as e:
            print(f'Error with {fronius_url}')
            print("Error receiving grid_sum from Fronius: ", e)
            grid_sum = 0 # Notausstieg
            wlanConnect()
        else:        
            grid_sum = int(data_fr['Body']['Data']['Site']['P_Grid'])
    
    setpoint    = 0     # Neues Limit in Watt

    # Setzt ein limit auf den Wechselrichter mit angegebener Seriennummer
    def setLimit(Serial, Limit):
        #Neben devcontrol geht es wohl auch mit topic/ctrl/limit_nonpersistent_absolute/inverter_id Wert.
        #DTU/ctrl/limit_nonpersistent_absolute/0 50
        #const uint16_t limitType_ANP = 0;	// AbsoluteNonPersistent
        #const uint16_t limitType_RNP = 1;	// RelativeNonPersistent
        #const uint16_t limitType_AP = 256;	// AbsolutePersistent
        #const uint16_t limitType_RP = 257;	// RelativePersistent
        headers = {'Content-Type': 'application/x-www-form-urlencoded'}
        payload = f'''data={{"serial":"{Serial}", "limit_type":0, "limit_value":{Limit}}}'''
        if debug: print(f'payload: {payload}')
        newLimit = requests.post(url=f'http://{dtuIP}/api/limit/config', data=payload, auth=(dtuNutzer, dtuPasswort), headers=headers)
        print('Konfiguration Status:', newLimit.json()['type'])
        # kurzes Doppelblinken der LED bei neuem Limit am WR
        blink_double()

    # Werte setzen
    print(f'aktueller Bezug - Haus: {grid_sum}')
    if influx: add('grid_sum',grid_sum)
    #watchdog.feed() # <- Watchdog füttern, kein Futter -> Reset
    gc.collect()
    print('free:{}, alloc:{}'.format(gc.mem_free()/1024, gc.mem_alloc()/1024))
    # nur für debug bei Nacht
    if debug: reachable = True
    if debug: producing = True
    if reachable: # wenn DTU nicht erreichbar ist, mach ich nix
        if producing:  # wenn es dunkel ist, mach ich auch nix
            # Setze den Grenzwert auf den höchsten Wert, wenn er über dem zulässigen Höchstwert liegt.
            if (altes_limit >= maximum_wr or grid_sum >= maximum_wr or setpoint >= maximum_wr):
            #if (grid_sum >= maximum_wr or setpoint >= maximum_wr):
                print("setze Limiter auf maximum")
                setpoint = maximum_wr
            # wird weniger bezogen als maximum_wr dann neues Limit ausrechnen
            if (grid_sum+altes_limit) <= maximum_wr:
                if altes_limit == maximum_wr:
                    setpoint = grid_sum
                    print("neues Limit runtergezogen auf grid_sum ",setpoint)
                else:
                    setpoint = grid_sum + altes_limit# - hysterese
                    print("setpoint:",grid_sum,"+",altes_limit)#,"-",hysterese)
                    print("neues Limit berechnet auf -->",setpoint)
            if grid_sum >= 0 and grid_sum <= hysterese:
                setpoint = altes_limit
                print("setpoint: ruhig bleiben, Limit bleibt wg Hysterese bei altem Limit",setpoint)
            if power <= hysterese and grid_sum <= hysterese:
                setpoint = altes_limit
                print("setpoint: kaum Solarleistung, Limit bleibt bei altem Limit", setpoint)
            if grid_sum <= hysterese and setpoint <= hysterese:
                setpoint = hysterese
                print("setpoint war zu klein, Limit auf hysterese eingestellt")
            #watchdog.feed() # <- Watchdog füttern, kein Futter -> Reset
            print("setze Einspeiselimit auf: ",setpoint)
            # neues limit setzen
            setLimit(serial, setpoint)
            print("Solarzellenstrom:",power,"  Setpoint:",setpoint)
            #watchdog.feed() # <- Watchdog füttern, kein Futter -> Reset
            #eventuell ausgeben an Influx-Datenbank 
            if influx:
                add('altes_limit',altes_limit)
                add('reachable',int(reachable))
                add('producing',int(producing))
                add('akt_p_dc',power_dc)
                add('voltage_dc',voltage_dc)
                add('akt_power',power)
                #add('grid_sum',grid_sum)
                add('setpoint',setpoint)
                add('yield_d',tagesleistung)
                add('yield_total', gesamtleistung)
                #add('name', benennung) Strings nicht in die INfluxDB schicken.
                add('irradiation', einstrahlung)
                add('temperature', temperature)
                add('data_age', data_age)
                if debug: print('data_age', data_age)
            i=0
            #  warten bis erneut Werte abgefragt werden
            while i <= wartezeit:
                print(".", end = '') # Wartepunkte horizontal ausgeben
                time.sleep(1)
                #watchdog.feed() # <- Watchdog füttern, kein Futter -> Reset
                i+=1
            print(" ")
        else:
            print(f"   --> Wechselrichter produziert nichts.")
    else:
        print(f"   --> Wechselrichter ist nicht erreichbar. Warte 60s...")
        time.sleep(60)

    # Wenn der Wechselrichter nicht erreicht werden kann, wird das limit auf maximum gestellt
    if setpoint == 0: setpoint = grid_sum
    if not reachable: 
        setpoint = maximum_wr
        add('reachable',0)
        add('producing',0)
        add('akt_p_dc',0)
        add('akt_power',0)   
