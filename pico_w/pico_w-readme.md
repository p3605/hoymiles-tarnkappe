## Erklärung in deutsch, da der Limiter eh nur im DACH-Raum Sinn macht.
Der micropython Code unterstützt das Einlesen der Hausverbrauchs von 
- Shelly 3EM
- Volkszähler
- Fronius Smartmeter
und es besteht eine Ausgabemöglichkeit an eine Influx-Datenbank.

## Anpassungen
In den ersten Zeilen des micropython codes lassen sich Anpassungen vornehmen.

![config area](/media/Auswahl_20230322_004.png "hier anpassen")

Tragt eure SSID und das WLan Passwort hier ein:

`wlanSSID = 'ssid'` 

`wlanPW = 'pass'` 



Wie eng der Limiter versuchen soll den Stromverbrauch an die Nulleinspeisung heranzuführen könnt ihr hier beeinflussen: 

`hysterese = 40  # in Watt` 

`wartezeit = 5   # in Sekunden` 

Mit hysterese lasst ihr etwas Spielraum um im Verbrauchsbereich zu bleiben. Der Limiter versucht im Beispiel dann auf einen Verbrauch von 40 Watt zu kommen.

Mit wartezeit reduziert ihr die Abfragen des Stromverbrauchs und das Setzen des Limits. Im angegebenen Beispiel wartet der Limiter 5 Sekunden zusätzlich zu den vorhandenen Anfrage- und Responsezeiten der angesprochenen Geräte

Mit True oder False (jeweils mit grossem Buchstaben am Anfang) stellt ihr ein, welcher Stromzähler verwendet werden soll.

`shelly = False`

`vz = True`

`fronius = False`


## Influx Datenbank
Mit True oder False stellt ihr ein, ob eine Ausgabe an InfluxDB erfolgen soll.

`influx = True` 

Wie heisst eure Datenbank in InfluxDB?

`database = 'dtu'`

Wie soll das Measurment in InfluxDB heissen?

`measurement_name = 'dtutest'`

Wie wird die InfluxDB erreicht?

`influx_url = 'http://192.168.22.210:8086/write?db='+database #&precision=m`


## openDTU Anpassungen
Hier stellt ihr die Details zur openDTU ein:

- `maximum_wr = 400`            # was kann das Modul / WR max abgeben
- `serial = "112174217264"`     # Seriennummer des Wechselrichters
- `dtuIP = '192.168.20.147'`    # IP Adresse von OpenDTU
- `dtuNutzer = 'admin'`         # OpenDTU Nutzername
- `dtuPasswort = 'openDTU42'`   # OpenDTU Passwort


## welche Daten werden von der DTU geholt?

Selektiert spezifische Daten aus der json response

gilt aktuell nur für Wechselrichter mit einem String

und nur für den ersten Wechselrichter in der DTU

- `reachable       = j_data['inverters'][0]['reachable']`                # ist DTU erreichbar ?
- `producing       = int(j_data['inverters'][0]['producing'])`           # produziert der Wechselrichter etwas ?
- `altes_limit     = int(j_data['inverters'][0]['limit_absolute'])`      # wo war das alte Limit gesetzt
- `power_dc        = j_data['inverters'][0]['AC']['0']['Power DC']['v']` # Lieferung DC vom Panel
- `voltage_dc      = j_data['inverters'][0]['DC']['0']['Voltage']['v']`  # Spannung DC vom Panel
- `power           = j_data['inverters'][0]['AC']['0']['Power']['v']`    # Abgabe BKW AC in Watt
- `tagesleistung   = j_data['total']['YieldDay']['v']`                   # Tagesleistung aller Strings
- `gesamtleistung  = j_data['total']['YieldTotal']['v']`                 # Gesamtleistung aller Strings


## Feldnamen in InfluxDB
- altes_limit
- reachable
- producing
- akt_p_dc
- voltage_dc
- akt_power
- grid_sum
- setpoint
- yield_d
- yield_total
